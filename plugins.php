<?php
$plugins = array(
    array(
        'plugin_key' => 'phalapi_alipay',
        'plugin_name' => 'PhalApi 2.x 支付宝插件',
        'plugin_author' => 'PhalApi官方',
        'plugin_price' => '0',
        'plugin_status' => 0, // 0未安装
        'plugin_version' => '1.0',
        'plugin_op' => '<a href="https://www.phalapi.net/download/plugins/phalapi_alipay.zip" target="_blank">免费下载</a>',
    ),
    array(
        'plugin_key' => 'phalapi_user',
        'plugin_name' => 'PhalApi 2.x User用户插件',
        'plugin_author' => 'PhalApi官方',
        'plugin_price' => '0',
        'plugin_status' => 0, // 0未安装
        'plugin_version' => '1.0',
        'plugin_op' => '<a href="https://www.phalapi.net/download/plugins/phalapi_user.zip" target="_blank">免费下载</a>',
    ),
    array(
        'plugin_key' => 'phalapi-theme-magician',
        'plugin_name' => 'PhalApi文档主题《Magician》',
        'plugin_author' => '安杰',
        'plugin_price' => '0',
        'plugin_status' => 0, // 0未安装
        'plugin_version' => '1.0',
        'plugin_op' => '<a href="https://www.phalapi.net/download/plugins/phalapi-theme-magician.zip" target="_blank">免费下载</a>',
    ),
    array(
        'plugin_key' => 'crypt_decrypt',
        'plugin_name' => 'PhalApi 2.x 加解密插件',
        'plugin_author' => 'yqq',
        'plugin_price' => '0',
        'plugin_status' => 0, // 0未安装
        'plugin_version' => '1.0',
        'plugin_op' => '<a href="https://www.phalapi.net/download/plugins/crypt_decrypt.zip" target="_blank">免费下载</a>',
    ),
    array(
        'plugin_key' => 'phalapi_portal',
        'plugin_name' => 'PhalApi 2.x Portal运营平台',
        'plugin_author' => 'PhalApi官方',
        'plugin_price' => '0',
        'plugin_status' => 0, // 0未安装
        'plugin_version' => '1.1',
        'plugin_op' => '<a href="https://www.phalapi.net/download/plugins/phalapi_portal.zip" target="_blank">免费下载</a>',
    ),
    array(
        'plugin_key' => 'phalapi_mini_tea',
        'plugin_name' => '【教程配套代码】茶店微信小程序商城源码',
        'plugin_author' => 'PhalApi官方',
        'plugin_price' => '0',
        'plugin_status' => 0, // 0未安装
        'plugin_version' => '1.0',
        'plugin_op' => '<a href="https://www.phalapi.net/download/plugins/phalapi_mini_tea_ALL.zip">免费下载</a>',
    ),
);

header('Content-Type:application/json; charset=utf-8');
echo json_encode(array('plugins' => $plugins, 'total' => 1));
exit();
