<?php

header('Content-Type:application/json;charset=utf-8');

$url = 'http://qa.phalapi.net/?/sort_type-new__day-0__is_recommend-0__page-1&for_phalapi=1';

$listType = !empty($_GET['list_type']) ? $_GET['list_type'] : '';

if ($listType == 'hot') {
    $url = 'http://qa.phalapi.net/?/sort_type-hot__day-30&for_phalapi=1';
} else if ($listType == 'todo') {
    $url = 'http://qa.phalapi.net/?/sort_type-unresponsive&for_phalapi=1';
}

$content = file_get_contents($url);

$data = json_decode($content, true);

$indexData = array();

if (!is_array($data)) {
    echo json_encode($indexData);die();
}

foreach ($data as $it) {
    if (empty($it['question_content'])) {
        continue;
    }

    $item = array(
        'url' => 'http://qa.phalapi.net/?/question/' . $it['question_id'],
        'title' => htmlspecialchars($it['question_content']),
        'update_time' => date('m-d H:i', $it['update_time']),
    );

    $indexData[] = $item;
}

$indexData = array_slice($indexData, 0, 10);

echo json_encode($indexData);die();

