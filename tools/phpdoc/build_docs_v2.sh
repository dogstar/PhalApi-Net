#!/bin/bash

## Env
#WIKI_PATH=$1
PHALAPI_NET_PATH="/home/dogstar/gitosc/PhalApi-Net"
BASE_PATH="/home/dogstar/gitosc/PhalApi-Net/tools/phpdoc"
PHPDOC_PATH="/usr/bin/phpdoc"

cd $BASE_PATH;

#清除旧的生成
rm ./_phalapi_docs_release_v2/* -rf;

$PHPDOC_PATH --ignore "*/Tests/*" --config=./phpdoc_v2.xml 

cp ./imgs/* ./_phalapi_docs_release_v2/images/;

rm $PHALAPI_NET_PATH/docs_v2/* -rf;
cp ./_phalapi_docs_release_v2/* $PHALAPI_NET_PATH/docs_v2/ -R;

cd /home/dogstar/gitosc/PhalApi-Net
git add --all
git commit -a -m "up"
git push


echo ""
echo "done!"
echo ""

