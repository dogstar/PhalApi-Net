#!/bin/bash

## Env
#WIKI_PATH=$1
PHALAPI_NET_PATH="/home/dogstar/gitosc/PhalApi-Net"
BASE_PATH="/home/dogstar/gitosc/PhalApi-Net/tools/phpdoc"
PHPDOC_PATH="/usr/bin/phpdoc"

cd $BASE_PATH;

#清除旧的生成
rm ./_phalapi_docs_release/* -rf;

$PHPDOC_PATH --ignore "*/Tests/*,/home/dogstar/gitosc/PhalApi/PhalApi/Tests/*" --config=./phpdoc.xml 

cp ./imgs/* ./_phalapi_docs_release/images/;

rm $PHALAPI_NET_PATH/docs/* -rf;
cp ./_phalapi_docs_release/* $PHALAPI_NET_PATH/docs/ -R;

cd /home/dogstar/gitosc/PhalApi-Net
git add --all
git commit -a -m "up"
git push


echo ""
echo "done!"
echo ""

