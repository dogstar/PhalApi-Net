#!/bin/bash
#
# ./build_wikis.sh /mnt/hgfs/F/PHP/PhalApi.wiki/
#
# @author dogstar 20150408
#

echo "not here~"
exit 1

## Usage
if [ $# -lt 1 ] 
then
    echo "Usage: $0 <PhalApi-Api-Wiki>"
    echo ""
    #exit 1
fi

## Env
#WIKI_PATH=$1
WIKI_PATH="/home/dogstar/gitosc/PhalApi-Api-Wiki/v2.0"
BASE_PATH=$(cd `dirname $0`; pwd)
BASE_PATH="/home/dogstar/gitosc/PhalApi-Net/tools/"
PHP_PATH="/usr/bin/php"

if [ ! -d $WIKI_PATH ]
then
    echo "Error: can not open $WIKI_PATH !"
    echo ""
    exit 2
fi

for line in $(ls $WIKI_PATH | grep md | grep -v "md~" )
do
    echo "build $line ..."
    $PHP_PATH $BASE_PATH/parse_markdown_file.php $WIKI_PATH/$line $BASE_PATH/../xiaobai xiaobai
done

cd $BASE_PATH/..

git add .
git commit -a -m "自动提交生成的HTML在线文档"
git push

echo ""
echo "OK!"
echo ""

