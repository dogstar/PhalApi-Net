#!/usr/bin/env php

<?php
/**
 * @author dogstar 20150408
 */

if ($argc < 3) {
    echo "Usage: $argv[0] <md_file> <save_folder> \n\n";
    exit(1);
}

$file = trim($argv[1]);
if (!file_exists($file)) {
    echo "Miss $file !\n\n";
    exit(2);
}

$md_type = isset($argv[3]) ? $argv[3]: 'wiki'; // wiki/book/xiaobai

require_once dirname(__FILE__) . '/Parsedown.php';

$Parsedown = new Parsedown();

$rs = $Parsedown->text(file_get_contents($file));

// 样式调整
$rs = str_replace('<table>', '<table class="table table-bordered">', $rs);

if ($md_type == 'wiki') {
$rs .= '<div style="float: left">
<h4>
<a href="http://qa.phalapi.net/">还有疑问？欢迎到社区提问！</a>
&nbsp;&nbsp;
<a href="http://docs.phalapi.net/#/v2.0/">切换到PhalApi 2.x 开发文档。</a>
</h4>
</div>';
}

$folder = trim($argv[2]);
if (!is_dir($folder)) {
    mkdir($folder, 0777, true);
}

$info = pathinfo($file);

//$title_maps = array(
//    'wiki' => '官方文档',
//    'book' => '初识PhalApi',
//    'xiaobai' => '小白接口',
//    'wiki2' => '官方文档2.x',
//);

// SEO优化

$wiki_active = $book_active = $xiaobai_active = '';

$header = file_get_contents(dirname(__FILE__) . '/header.html');
$sub_nav = '';

if ($md_type == 'xiaobai') {
    $header = str_replace('{keywords}', ',小白接口,免费接口,免费后端接口,元数据,集合数据,计数器', $header);

    $filename_maps = array(
        'README' => '产品简介',
        'register' => '开通指南(图文)',
        'tutorial' => '接入指南',
        'meta_set_counter' => '三大数据',
        'user' => '用户模块',
        'user_meta' => '用户元数据模块',
        'user_set' => '用户集合模块',
        'user_counter' => '用户计数器',
        'main_meta' => '应用元数据模块',
        'main_set' => '应用集合模块',
        'main_counter' => '应用计数器',
        'util_cdn' => 'CDN模块',
        'util_logger' => '日志上报',
        'faq' => '常见问题',
        'support' => '联系我们',
        'debug' => '在线调试',
        'demo_note' => '开发示例：迷你日记',
        'demo_book' => '开发示例：包车预约',
    );

    $my_title = isset($filename_maps[$info['filename']]) ? $filename_maps[$info['filename']] : $info['filename'];
    $header = str_replace('{title}', $my_title, $header);
    $header = str_replace('<title>', '<title>' . $my_title . ' - 小白接口 | ', $header);

    $sub_nav = '        <nav class="navbar navbar-default ">
        <div class="container">
        <div id="navbar">
        <ul class="nav navbar-nav">
        <li><a target="_blank" href="http://docs.api.phalapi.net/#/v2.0/README">小白首页</a></li>
        <li><a target="_blank" href="http://docs.api.phalapi.net/#/v2.0/README?id=%e6%bc%94%e7%a4%ba%e7%89%88">演示版</a></li>
        <li><a target="_blank" href="http://admin.api.phalapi.net/">小白后台</a></li>
        <li><a target="_blank" href="http://api.phalapi.net/docs.php?type=fold">查看接口</a></li>
        <li><a target="_blank" href="http://admin.api.phalapi.net/index.php?r=user/registration&from=nav">免费开通</a></li>
        <!--
        <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
        <ul class="dropdown-menu">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        </ul>
        </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
        <li><a href="#">Link</a></li>
        <li><a href="#">Link</a></li>
        </ul>
        -->
        </div><!--/.nav-collapse -->
        </div>
        </nav>';

    $xiaobai_active = 'class="active"';
} else if ($md_type == 'book') {
    $header = str_replace('{keywords}', ',初识PhalApi,电子书,探索接口服务开发的技艺,接口服务,开发技艺,免费电子书,PhalApi电子书,PhalApi书籍', $header);

    $filename_maps = array(
        'introduction' => '前言',
        'chapter1' => '第2章 基础入门探索',
        'ch-1-yu-jian-phalapi' => '第1章 遇见PhalApi',
        'ch-2-ji-chu-ru-men' => '第2章 基础入门',
        'ch-3-gao-ji-zhu-ti' => '第3章 高级主题',
        'ch-4-not-only-coding' => '第4章 不只是编码',
        'chapter2' => '第二部分 项目案例',
        'ch-5-new-project' => '第5章 全新的创业项目',
        'ch-6-chong-xue' => '第6章 重写历史遗留项目',
        'ch-7-perfect-project' => '第7章 一个极致的项目',
        'chapter3' => '第三部分 再进一步',
        'ch-8-what-is-phalapi' => '第8章 PhalApi完美诠释',
        'ch-9-how-to-desgin-api' => '第9章 如何有效设计接口框架',
        'ch-10-open-source' => '第10章 开源这条路',
    );
    $my_title = isset($filename_maps[$info['filename']]) ? $filename_maps[$info['filename']] : $info['filename'];
    $header = str_replace('{title}', $my_title, $header);
    $header = str_replace('<title>', '<title>' . $my_title . ' - 初识PhalApi | ', $header);

    $sub_nav = '        <nav class="navbar navbar-default ">
        <div class="container">
        <div id="navbar">
        <ul class="nav navbar-nav">
        <li><a target="_blank" href="https://www.phalapi.net/book/">《初识PhalApi》</a></li>
        <li><a target="_blank" href="http://www.ituring.com.cn/book/2405">图灵社区</a></li>
        <li><a target="_blank" href="https://www.gitbook.com/book/phalapi/meet_phalapi/details">Gitbook</a></li>
        <li><a target="_blank" href="https://github.com/phalapi/meet">Github</a></li>
        </div><!--/.nav-collapse -->
        </div>
        </nav>';

    $book_active = 'class="active"';
} else {
    $header = str_replace('{keywords}', ',PHP接口框架,phalapi文档,phalapi wiki,PhalApi文档,phalapi在线文档,phalapi官方文档', $header);
    $header = str_replace('{title}', $info['filename'], $header);
    $header = str_replace('<title>', '<title>' . ($info['filename'] != 'index' ? $info['filename'] : '官方文档') . ' | ', $header);

    $wiki_active = 'class="active"';
}

// 高亮导航
$header = str_replace(array('{wiki_active}', '{book_active}', '{xiaobai_active}'), array($wiki_active, $book_active, $xiaobai_active), $header);

$footer = file_get_contents(dirname(__FILE__) . '/footer.html');

$_ds_id = md5($info['filename']);
$_ds_title = $info['filename'];
$_ds_url = 'http://www.phalapi.net/wikis/' . urlencode($info['filename']) . '.html';

$adHtml = '';

$content = sprintf(
    "%s
    <div id=\"content\">
        <div class=\"container\">

            %s

            <div class=\"row row-md-flex row-md-flex-wrap\">
                %s
                %s
            </div>
        </div>
    </div>

 %s",
    $header, $sub_nav, $adHtml, $rs , $footer 
);

file_put_contents($folder . '/' . $info['filename'] . '.html', $content);

